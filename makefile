#Author: A.R. Cowie
#Description:  My standard template for makefiles
#

SUBDIRS = $(shell ls -d */)

all:
	for dir in $(SUBDIRS) ; do\
		make -C $$dir ; \
	done
