#!/usr/bin/env python
#author = A. R. Cowie
import csv
import pandas as pd
import matplotlib.pyplot as plt 

def procFile(fileName):
    msgTime = []
    msgSize = []
    bandwidth = []

    with open(fileName) as csvPingPondData:
        csvData = csv.reader(csvPingPondData)
        for row in csvData:
            msgSize.append(float(row[0])/1000)
            msgTime.append(float(row[1]))
            bandwidth.append((float(row[0])/float(row[1]))/1000000)

    return msgTime, msgSize, bandwidth

msgTime, msgSize, bandwidth = procFile("pingPongResults.csv")

#print(msgSize, msgTime, bandwidth)
temp = {"Msg Size" : msgSize, "Msg Time": msgTime, "Bandwidth": bandwidth}
pingPongDF = pd.DataFrame(temp)

print(pingPongDF.describe(include = "all"))

f1Ax1 = pingPongDF.plot(x = "Msg Size", y = "Msg Time")
f1Ax1 = plt.ylabel("Msg Time(sec)", fontsize = 20)
f1Ax1 = plt.xlabel("Msg Size(kB)", fontsize = 20)
f1Ax1 = plt.title("Msg Time vs Msg Size(On CCR)", fontsize = 20)
plt.savefig("msgSizeVsMsgTime.png")
plt.close()


f1Ax2 = pingPongDF.plot(x = "Msg Size", y = "Bandwidth")
f1Ax2 = plt.ylabel("Bandwidth(MB/sec)", fontsize = 20)
f1Ax2 = plt.xlabel("Msg Size(kB)", fontsize = 20)
f1Ax2 = plt.title("Bandwidth vs Msg Size(On CCR)", fontsize = 20)
plt.savefig("bandwidthVsMsgSize.png")
plt.close
    

