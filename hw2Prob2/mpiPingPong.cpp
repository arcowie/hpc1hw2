/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mpiPingPong.cpp

* Purpose :

* Creation Date : 24-11-2017

* Last Modified : Fri 01 Dec 2017 12:28:43 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include <mpi.h>
#include "ChkMPIErr.h"
#include <fstream>
#include <iomanip>

using namespace std;


int main(int argc, char *argv[]){


	int rank, size, ierr, repeats=200, maxBuffSize=1000000, stepSize=10000;

	double timeBegin=0, timeEnd=0, timeTotal=0;
	
	//test message is allocated on the heap do to its size.
	double *testBuff = new double[maxBuffSize];
	
	ofstream results;
	
	//place values into the test message
	//for(int i = 0; i < maxBuffSize; i++){
	//	testBuff[i] = 1.0;
//	}
		
	ierr=MPI_Init(&argc, &argv); chkMPIErr(ierr, rank, MPIInitErr, MPI_COMM_WORLD);
	
	ierr=MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);chkMPIErr(ierr, rank, MPIErrHandErr, MPI_COMM_WORLD);

	ierr=MPI_Comm_rank(MPI_COMM_WORLD, &rank);chkMPIErr(ierr, rank, MPICommRankErr , MPI_COMM_WORLD);

	ierr=MPI_Comm_size(MPI_COMM_WORLD, &size);chkMPIErr(ierr, rank, MPICommSizeErr, MPI_COMM_WORLD);
	

	//Check to make sure that only 2 processors are being used
	if(size!=2){
		cout << "\n**************ERROR******************\n";
		cout << "This benchmark program is designed for to be used with 2 processors.\n";
		cout << "Please rerun this program with 2 processors.\n";
		MPI_Abort(MPI_COMM_WORLD, 1);
		MPI_Finalize();
	}
	
	//Prep for file writing and for display output, only proc rank 0
	if(rank==0){
		results.open("pingPongResults.csv");
		cout << "\n************Begining PingPong Benchmark****************\n";
		cout << "Msg Size(bytes)" << setw(15) << "Time(s)" << setw(15) << "Repeats\n";
	}
	for(int i = 0; i < maxBuffSize; i += stepSize){
		timeTotal=0;
		for(int j = 0; j < repeats; j++){
			timeBegin=MPI_Wtime();
			
			if(rank==0){
				ierr=MPI_Send(testBuff, i+1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD); chkMPIErr(ierr, rank, MPISendErr, MPI_COMM_WORLD);
				ierr=MPI_Recv(testBuff, i+1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);chkMPIErr(ierr, rank, MPIRecErr, MPI_COMM_WORLD);
 			}

			else{
				ierr=MPI_Recv(testBuff, i+1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);chkMPIErr(ierr, rank, MPIRecErr, MPI_COMM_WORLD);
 				ierr=MPI_Send(testBuff, i+1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD); chkMPIErr(ierr, rank, MPISendErr, MPI_COMM_WORLD);
			}
			
			timeEnd=MPI_Wtime();
			timeTotal+=timeEnd-timeBegin;

			MPI_Barrier(MPI_COMM_WORLD);
		}
		
		if(rank==0){
				cout << "\n" << setw(5) << sizeof(double)*(i+1) << setw(20)  << timeTotal/repeats << setw(10) << repeats << "\n";
			//	results << setw(5)<< sizeof(double)*(i+1) << setw(28) << timeTotal/repeats << "\n";	
				results << sizeof(double)*(i+1)	<< "," << timeTotal/repeats	<< "\n";
			}

	
	}
	
//	MPI_Barrier(MPI_COMM_WORLD);
	
	if(rank==0){results.close();}

	ierr=MPI_Finalize();chkMPIErr(ierr, rank, MPIFinalErr, MPI_COMM_WORLD);

	return 0;

}
