/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mpiHelloWorld.cpp

* Purpose :

* Creation Creation Date : 17-10-2017

* Last Modified : Fri 24 Nov 2017 12:58:08 PM EST

* Created By :  Albert R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include <mpi.h>
#include "ChkMPIErr.h"
using namespace std;


int main(int argc, char *argv[]){

int rank, numProc, ierr;




ierr=MPI_Init(&argc, &argv);chkMPIErr(ierr, rank, MPIInitErr, MPI_COMM_WORLD);

ierr=MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);chkMPIErr(ierr, rank, MPIErrHandErr, MPI_COMM_WORLD);

ierr=MPI_Comm_rank(MPI_COMM_WORLD, &rank);chkMPIErr(ierr, rank, MPICommRankErr, MPI_COMM_WORLD);

ierr=MPI_Comm_size(MPI_COMM_WORLD, &numProc);chkMPIErr(ierr, rank, MPICommSizeErr, MPI_COMM_WORLD);

cout <<"\nHello world from process " << rank <<" out of " << numProc<<"\n";


ierr=MPI_Finalize(); chkMPIErr(ierr, rank, MPIFinalErr, MPI_COMM_WORLD);

return 0;

}
