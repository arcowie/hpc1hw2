/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : ChkMPIErr.h

* Purpose :  Handle MPI errors

* Creation Creation Date : 22-11-2017

* Last Modified : Fri 01 Dec 2017 12:44:26 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#ifndef ChkMPIErr_h
#define ChkMPIErr_h
#include <string>
#include <mpi.h>



/**
 * @Description Set of definitions maping to an int.  This is used by ChkMPIErr to return a more meaningful message
 */

#define MPIInitErr        1
#define MPICommRankErr    2
#define MPICommSizeErr    3
#define MPISendErr        4
#define MPIRecErr         5
#define MPIFinalErr       6
#define MPIErrHandErr     7 
#define MPICreatStructErr 8
#define MPICommitErr      9
#define MPIGetAddressErr  10
#define MPIBcastErr       11
#define MPIReduceErr      12
/**
 * @Description Function handles errors returned by different MPI functions.  The function
 *              evaluates the error and outputs a string identifing the translated error code
 *              as well as he function that caused the error.  At the end the function aborts
 *
 * @param ierr  Type: int, this is the value returned by the MPI function, and used to determin
 *                    the error class and string.
 * @param errSource Type: int, this corresponds to the error source with the value of the int corresponding
 *                        to the #define string.
 * @param com Type: MPI_Comm, The MPI communicator assocaited with the proc that had the error.
 *
 * @return Type: void
 */
extern void chkMPIErr(int ierr, int rank, int errSource, MPI_Comm comm);

/**
 * @Description Function takes one of the error source codes and returns the appropriate string.
 *
 * @param getErrSource Type: int, corresponds to the errSource number passed to chkMPIErr.
 *
 * @return Type: string, the string that corresponds to the error source.
 */
extern std::string getErrSource(int errSource);

#endif
