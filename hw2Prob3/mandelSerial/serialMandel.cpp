/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : serialMandel.cpp

* Purpose :  Serial, i.e. nonparallel calculator of the Mandelbrot set

* Creation Date : 01-12-2017

* Last Modified : Fri 01 Dec 2017 11:38:53 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include "mandelLib.h"


using namespace std;

int evalMand(mandInfo *m, int a, double *area){


	int numPtsRe, numPtsIm, numPtsTot, ierr, temp = 0;
	

	//setup for writing output to a file.  Used a stringstream to convert an int, rank, to a string
	string outputFileName;
	stringstream ss;
	ss << "Prob3Serial" << gridSize(a) << ".csv";
	outputFileName = ss.str();


	//.open requires a c style string hence .c_str() method
	ofstream outfile;
	outfile.open(outputFileName.c_str(),  ofstream::out);


	//calculate total number of points
	numPtsRe=(int)((m->maxRe-m->minRe)/m->stepRe);
	numPtsIm=(int)((m->maxIm-m->minIm)/m->stepIm);
	numPtsTot=numPtsRe*numPtsIm;

	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsTot];

	//Loops through the points like a matrix a row at at time
	for(double i=m->minIm; i<=m->maxIm;i+=m->stepIm){
		for(double j=m->minRe; j<=m->maxRe; j+=m->stepRe ){
			
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], m->maxIter);
	//		cout << "\n" << pointsLocal[temp] << "\n";
			outfile << pointsLocal[temp] << "\n";
			if(pointsLocal[temp].member==1){area[0]+=(m->stepIm*m->stepRe);}
			temp++;
		}
	}



	outfile.close();

	return 0;
}



int main(int argc, char *argv[]){

	int ierr;
	double area[3]={0};
	mandInfo m;

	if(argc==1){
		m.maxIter = 100;
		m.stepRe  = 0.01;
		m.stepIm  = 0.01;
		m.maxRe   = 2.0;
		m.minRe   = -2.0;
		m.maxIm   = 1.0;
		m.minIm   = -1.0;
	}

	mandPt setUpPt;

	for(int i = 0; i<3; i++){
		ierr = evalMand(&m, i, area+i);
		if(ierr != 0){cout << "\nError in for loop from evalMand()\n";}
		m.stepRe=m.stepRe*2;
		m.stepIm=m.stepIm*2;
	}
		cout << "\nArea: " << area[0] << " " << area[1] << " " << area[2] << "\n";
	
		ofstream sumFile;
		sumFile.open("Prob3SummaryFileSerial.csv");
	
		sumFile << "\n" << area[0] << "," << area[1] << "," << area[2] << "\n";
		
		sumFile.close();
}	


