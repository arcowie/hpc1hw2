#!/usr/bin/env python

#######################################################
# File Name : mandBrotDataProc.py
                                                     
#Purpose : Concatonate the output from different PE's
#          and plot the data

#Creation Date : 29-11-2017

#Author :  A.R. Cowie
########################################################

import pandas as pd
import glob
import matplotlib.pyplot as plt
import os

def procData(method, gridSize):
    list_ = []

    files = glob.glob("./Prob3"+method+"*"+gridSize+".csv")

    for csvFile in files:
        dF = pd.read_csv(csvFile, index_col=None, names = ["Real", "Imaginary", "Loops", "Member"])
        list_.append(dF)

    dFrame = pd.concat(list_)
    
    colVec= ["Yellow" if mem == 1 else "Blue" for mem in dFrame["Member"]]
    dFrame.plot(c=colVec, kind = "scatter", x = "Real", y = "Imaginary")
    plt.title(method+" "+gridSize)
    plt.savefig("Prob3Plot"+method+gridSize+".png")
    plt.show()
    
def cleanup():
    files = glob.glob("./*.csv")
    for f in files:
        os.remove(f)


procData("RoundRob", "Fine")
procData("RoundRob", "Medium")
procData("RoundRob", "Coarse")
procData("Block", "Fine")
procData("Block", "Medium")
procData("Block", "Coarse")
procData("Serial", "Fine")
procData("Serial", "Medium")
procData("Serial", "Coarse")
cleanup()
