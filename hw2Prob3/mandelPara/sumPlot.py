#!/usr/bin/env python

#######################################################
# File Name : sumPlot.py
                                                     
#Purpose :  Used to plot answers to the questions about the area of the set and effect of PE's on time

#Creation Date : 02-12-2017

#Author :  A.R. Cowie
########################################################
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

x=[2,4,24]
yBLockFine=[0.31948,0.238739,0.0638411]
yBlockMed=[0.0357599,0.0289621,0.118555]
yBlockCoarse=[0.00816202,0.00837994,0.122781]

yRobinFine=[0.15201,0.072664,0.115403]
yRobinMed=[0.0187109,0.00744486,0.127754]
yRobinCoarse=[0.00701094,0.00706506,0.122278]

x2 = [0.0001, 0.0009, 0.0081]

yRoundRobin = [1.5444, 1.5264, 1.5552]
yBlock = [1.5478, 1.5435, 1.6686]


redLine = mpatches.Patch(color = 'red' , label = 'Medium Grid')
blueLine = mpatches.Patch(color = 'blue' , label = 'Fine Grid')
greenLine = mpatches.Patch(color = 'green' , label = 'Coarse Grid')

plt.figure(figsize=(11,10))
sub1 = plt.subplot(3,1,1)
fine,   = plt.plot(x, yBLockFine, 'b')
medium, = plt.plot(x, yBlockMed, 'r')
coarse, = plt.plot(x, yBlockCoarse, 'g')
plt.legend([fine, medium, coarse],['Fine Grid','Medium Grid','Coarse Grid'])
plt.title('Block/grid Method')
sub1.set_xlabel('Number of Processors')
sub1.set_ylabel('Time(sec)')


sub2 = plt.subplot(3,1,2)
fine,   = plt.plot(x, yRobinFine, 'b')
medium, = plt.plot(x, yRobinMed, 'r')
coarse, = plt.plot(x, yRobinCoarse, 'g')
plt.legend([fine, medium, coarse],['Fine Grid','Medium Grid','Coarse Grid'],loc=4)
plt.title('Round Robin Method')
sub2.set_xlabel('Number of Processors')
sub2.set_ylabel('Time(sec)')

fig = plt.subplot(3,1,3)
block, = plt.plot(x2, yBlock, 'b')
robin, = plt.plot(x2, yRoundRobin, 'r')
plt.legend([block, robin], ['Block/Grid Method', 'Round Robin Method'], loc=0)
plt.title('Area of Mandelbrot set vs Grid Size')
fig.set_xlabel('Grid size')
fig.set_ylabel('Area of Mandelbrot set')


plt.tight_layout()

plt.savefig("timePlotandAreaPlot.png")

plt.show()




