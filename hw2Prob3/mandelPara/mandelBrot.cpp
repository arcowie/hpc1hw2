/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mandelBrot.cpp

* Purpose : Generate the Mandelbrot set using MPI

* Creation Creation Date : 18-11-2017

* Last Modified : Sun 03 Dec 2017 01:01:29 PM EST

* Created By :  Albert R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/
#include <iostream>
#include <mpi.h>
#include "ChkMPIErr.h"
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include "mandelLib.h"

using namespace std;


int block(mandInfo *m, int rank, int size, int a, double *area){

	int numPtsRe, numPtsIm, numPtsLocal, ierr, temp = 0;
	/* 
	 * realLoopParams[0] is the starting point for the real axis for the PE
	 * realLoopParams[1] is the max value with regards to the real axis for the PE
	 * realLoopParams[2] is the step size with regards to the real axis for the PE
	 */
	 
	double *realLoopParams = new double[3];
 
	//calculate total number of points
	numPtsRe=(int)((m->maxRe-m->minRe)/(size))/m->stepRe;
	numPtsIm=(int)(m->maxIm-m->minIm)/m->stepIm;
	numPtsLocal=numPtsRe*numPtsIm;


	//Array containg the factors for looping through the real part of the complex number
	realLoopParams[0] = m->minRe+(rank*numPtsRe*m->stepRe);
	realLoopParams[1] = realLoopParams[0]+(numPtsRe*m->stepRe);
	realLoopParams[2] = m->stepRe; 
	
	//Array containg the factors for looping through the imaginary part of the complex number
	
	//setup for writing output to a file.  Used a stringstream to convert an int, rank, to a string
	string outputFileName;
	stringstream ss;
	ss << "Prob3Block" << rank << gridSize(a) << ".csv";
	outputFileName = ss.str();


	//.open requires a c style string hence .c_str() method
	ofstream outfile;
	outfile.open(outputFileName.c_str(),  ofstream::out);



	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsLocal];
	
	//Loops through the points like a matrix a row at at time
	for(double i=m->minIm; i<m->maxIm;i+=m->stepIm){
		for(double j=realLoopParams[0]; j<realLoopParams[1]; j+=m->stepRe ){
			
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], m->maxIter);
	//		outfile << pointsLocal[temp] << "\n";
			if(pointsLocal[temp].member==1){area[0]+=(m->stepIm*m->stepRe);}
			temp++;
		}
	}

	//close the output file for this PE, grid size, and method of point selection.
	outfile.close();
	delete[] realLoopParams;
	//delete[] pointsLocal;
	return 0;
}

/**
 * @Description This function calculates the Mandelbrot set using a round robin
 * 				technique for determining which points each PE handles
 *
 * @param m Type: struct of mandInfo, contains the information for constructing the grid.
 * @param rank Type: int, the rank of the PE calling the function.
 * @param size Type: int, number of PE total.
 * @param round Type: int, identifies which round of calculations to identify fine, medium, coarse
 * @param area Type: double, the total area of members of the Mandelbrot set for this PE
 * 
 * @return Type: int, and error code, 0 means no error.
 */


int roundRobin(mandInfo *m, int rank, int size, int a, double *areaRobin){

	double area;
	int numPtsRe, numPtsIm, numPtsTot, numPtsLocal, ierr, temp = 0;
	

	//setup for writing output to a file.  Used a stringstream to convert an int, rank, to a string
	string outputFileName;
	stringstream ss;
	ss << "Prob3RoundRob" << rank << gridSize(a) << ".csv";
	outputFileName = ss.str();


	//.open requires a c style string hence .c_str() method
	ofstream outfile;
	outfile.open(outputFileName.c_str(),  ofstream::out);


	//calculate total number of points
	numPtsRe=(int)((m->maxRe-m->minRe)/m->stepRe);
	numPtsIm=(int)((m->maxIm-m->minIm)/m->stepIm);
	numPtsTot=numPtsRe*numPtsIm;

	//calculate how many points each PE must evaluate
	if(rank<(numPtsTot%size)){numPtsLocal=(int)((numPtsTot/size)+1);}
	else{numPtsLocal=(int)(numPtsTot/size);}

	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsLocal];

	//Loops through the points like a matrix a row at at time
	for(double i=m->minIm+(rank*m->stepIm); i<m->maxIm;i+=size*m->stepIm){
		for(double j=m->minRe+(rank*m->stepRe); j<m->maxRe; j+=size*m->stepRe ){
			
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], m->maxIter);
	//		cout << "\n" << pointsLocal[temp] << "\n";
	//		outfile << pointsLocal[temp] << "\n";
			if(pointsLocal[temp].member==1){areaRobin[0]=areaRobin[0]+(m->stepIm*m->stepRe);}
		
		//	cout << "\nAREA: " <<area<< " rank: " << rank<<"\n";
			temp++;
		}
	}


	//close the output file for this PE, grid size, and method of point selection.
	outfile.close();

	return ierr;
}



/*************START MAIN*****************/
int main(int argc, char **argv){


	int rank, size, ierr;
	double areaBlock[3]={0}, areaRobin[3]={0}, areaFinBlock[3]={0}, areaFinRobin[3]={0}, gridSize[3]={0};
	double robTimeInit[3]={0}, blockTimeInit[3]={0}, robTime[3]={0}, blockTime[3]={0};
	mandInfo m;

//	long int setUpPtAddress, ReAddress, ImAddress, iterAddress, memberAddress;
//	long int ReOffset, ImOffset, iterOffset, memberOffset;

	
	//assigns default values for the mesh if not provided at the command prompt
	if(argc==1){
		m.maxIter = 100;
		m.stepRe  = 0.01;
		m.stepIm  = 0.01;
		m.maxRe   = 2.0;
		m.minRe   = -2.0;
		m.maxIm   = 1.0;
		m.minIm   = -1.0;
	}
	mandPt setUpPt;

//	MPI_Status status;


	ierr=MPI_Init(&argc, &argv);chkMPIErr(ierr, rank, MPIInitErr, MPI_COMM_WORLD);
	
	ierr = MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);	

/*	deprecated code that I was going to use to have each PE write in parallel but ran out of time.
 *
 *	MPI_Datatype mandPtType;
	
	ierr=MPI_Get_address(&setUpPt, &setUpPtAddress);chkMPIErr(ierr, rank, MPIGetAddressErr, MPI_COMM_WORLD);
	ierr=MPI_Get_address(&setUpPt.Re, &ReAddress);chkMPIErr(ierr, rank, MPIGetAddressErr, MPI_COMM_WORLD);
	ierr=MPI_Get_address(&setUpPt.Im, &ImAddress);chkMPIErr(ierr, rank, MPIGetAddressErr, MPI_COMM_WORLD);
	ierr=MPI_Get_address(&setUpPt.iter, &iterAddress);chkMPIErr(ierr, rank, MPIGetAddressErr, MPI_COMM_WORLD);
	ierr=MPI_Get_address(&setUpPt.member, &memberAddress);chkMPIErr(ierr, rank, MPIGetAddressErr, MPI_COMM_WORLD);


	ReOffset=ReAddress-setUpPtAddress;
	ImOffset=ImAddress-setUpPtAddress;
	iterOffset=iterAddress-setUpPtAddress;
	memberOffset=memberAddress-setUpPtAddress;


	//array holding the list of MPI datatypes in the order they occur in the struct. 
	MPI_Datatype type[4]={MPI_DOUBLE, MPI_DOUBLE, MPI_INT, MPI_INT};
    
	//Array holding address for offset purposes
	MPI_Aint displ[4]={ReOffset, ImOffset, iterOffset, memberOffset};

	//array holding the lengths of the datatypes, also in order the occur.
	//note that sizeof(datatype) is used to make the code more platform independent.
	int blockLen[4]={sizeof(double), sizeof(double), sizeof(int), sizeof(int)};


	//Creates the new MPI_Datatype
	ierr = MPI_Type_create_struct(4, blockLen, displ, type, &mandPtType);chkMPIErr(ierr, rank, MPICreatStructErr, MPI_COMM_WORLD);

	//Required before the type can be used with a communicator
	ierr = MPI_Type_commit(&mandPtType);chkMPIErr(ierr, rank, MPICommitErr, MPI_COMM_WORLD);
*/

	ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);chkMPIErr(ierr, rank, MPICommRankErr, MPI_COMM_WORLD);
	
	ierr = MPI_Comm_size(MPI_COMM_WORLD, &size);chkMPIErr(ierr, rank, MPICommSizeErr, MPI_COMM_WORLD);
	

	if(((m.maxRe-m.minRe)/size)<=m.stepRe*9){cout << "\nError too mand PE\n";}

	for(int i = 0; i<3; i++){
		//Call to the roundRobin subroutine for calculating the Mandlebrot set
		robTimeInit[i] = MPI_Wtime();
		ierr = roundRobin(&m, rank, size, i, areaRobin + i);
		robTimeInit[i] = MPI_Wtime() - robTimeInit[i];
		//if(ierr != 0){cout << "\nError in for loop from roundRobin()\n";}


		blockTimeInit[i] = MPI_Wtime();
		//Call to the block subroutine for calculating the Mandlebrot set
		ierr = block(&m, rank, size, i, areaBlock + i);
		blockTimeInit[i] = MPI_Wtime() - blockTimeInit[i];
		if(ierr != 0){cout << "\nError in for loop from block()\n";}
		
		gridSize[i] = m.stepIm*m.stepRe;
		
		m.stepRe=m.stepRe*3;
		m.stepIm=m.stepIm*3;
		
	}
	
	//MPI_Reduce used to sum the local areas calculated by the individual PE's.
	ierr=MPI_Reduce(&areaBlock, &areaFinBlock, 3 , MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);
	
	ierr=MPI_Reduce(&areaRobin, &areaFinRobin, 3 , MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);

	//MPI_Reduce used to find the max time/the PE that too the most time to calculate its part of the datatset.
	ierr=MPI_Reduce(&robTimeInit, &robTime, 3 , MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);

	ierr=MPI_Reduce(&blockTimeInit, &blockTime, 3 , MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);




	
	//PE 0 was chosen to receive and write the data to a csv formatted file Prob3SummaryFile.csv.
	if(rank==0){
		
		cout << "\nNumber of PE's: " << size << " " << size << " " << size << "\n";
		cout << "\nFine Grid: " << gridSize[0] << " Medium Grid: " << gridSize[1] << " Coarse Grid: " << gridSize[2] << "\n";
		cout << "\nArea Robin: " << areaFinRobin[0]*size << " " << areaFinRobin[1]*size << " " << areaFinRobin[2]*size << "\n";
		cout << "\nArea Block: " << areaFinBlock[0] << " " << areaFinBlock[1] << " " << areaFinBlock[2] << "\n";
		cout << "\nRobin Time: " << robTime[0] << " " << robTime[1] << " " << robTime[2] << "\n";
		cout << "\nBlock Time: " << blockTime[0] << " " << blockTime[1] << " " << blockTime[2] << "\n";


		ofstream sumFile;
		sumFile.open("Prob3SummaryFile.csv");
	
		sumFile << size << "," << size << "," << size << "\n";
		sumFile << areaFinRobin[0] << "," << areaFinRobin[1] << "," << areaFinRobin[2] << "\n";
		sumFile << areaFinBlock[0] << "," << areaFinBlock[1] << "," << areaFinBlock[2] << "\n";
		sumFile << gridSize[0] << ", " << gridSize[1] << "," << gridSize[2]<<"\n";
		sumFile << robTime[0] << "," << robTime[1] << "," << robTime[2] << "\n";
		sumFile << blockTime[0] << "," << blockTime[1] << "," << blockTime[2] << "\n";
		sumFile.close();
	}	
	
	//cout << "\nRank: " <<rank <<" Area Robin: " << areaFinRobin[0] << " " << areaFinRobin[1] << " " << areaFinRobin[2] << "\n";
	ierr=MPI_Finalize();chkMPIErr(ierr, rank, MPIFinalErr, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIFinalErr, MPI_COMM_WORLD);


	return ierr;

}
